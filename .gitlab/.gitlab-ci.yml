# https://docs.gitlab.com/ee/ci/yaml/README.html#workflowrules-templates
include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml

default:
  tags:
    - gitlab-org

variables:
  # Image built using https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent-ci-image
  # and pushed into https://hub.docker.com/repository/docker/gitlab/gitlab-agent-ci-image
  BUILD_IMAGE_NAME: "gitlab/gitlab-agent-ci-image"
  # must use image digest to invalidate cache if image is updated.
  # This SHA was produced in https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent-ci-image/-/jobs/2546145535
  BUILD_IMAGE_SHA: "002ab2c29d209fb83054518d837f6e99e3c68f4f1639514bfc8b6191e89dc737"
  SECURE_ANALYZERS_PREFIX: "registry.gitlab.com/gitlab-org/security-products/analyzers"
  CS_MAJOR_VERSION: 2

stages:
  - test
  - scan
  - push_image
  - create_release

.bazel_build:
  image:
    name: "$BUILD_IMAGE_NAME@sha256:$BUILD_IMAGE_SHA"
    entrypoint: [""]
  before_script:
    - echo "build --verbose_failures" >> .bazelrc
    - echo "build --curses=no" >> .bazelrc
    - echo "build --show_timestamps" >> .bazelrc
    - |
      if [[ -f "$GOOGLE_APPLICATION_CREDENTIALS" ]]; then
        echo "build --google_default_credentials" >> .bazelrc
        echo "build --remote_cache=https://storage.googleapis.com/gitlab-kubernetes-test-bucket/$(cat .bazelversion)-$BUILD_IMAGE_SHA" >> .bazelrc
        echo "build --remote_download_minimal" >> .bazelrc
        echo "run --remote_download_outputs=toplevel" >> .bazelrc
      fi
    # https://docs.buildbuddy.io/docs/guide-metadata/
    - |
      if [ -n "$BUILDBUDDY_API_KEY" ]; then
        echo "build --build_metadata=VISIBILITY=PUBLIC" >> .bazelrc
        echo "build --bes_results_url=https://app.buildbuddy.io/invocation/" >> .bazelrc
        echo "build --bes_backend=grpcs://cloud.buildbuddy.io" >> .bazelrc
        echo "build --remote_header=x-buildbuddy-api-key=$BUILDBUDDY_API_KEY" >> .bazelrc
      fi
    - echo "test --test_output=all" >> .bazelrc
    - echo "test --test_arg=-test.v" >> .bazelrc
  #    - echo "build --sandbox_base=/dev/shm" >> .bazelrc # disabled because it's not big enough

.registry_creds: &registry_creds
  - mkdir -p "$HOME/.docker"
  - |
    credentials=$(echo -n "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" | base64)
    cat << EOF > "$HOME/.docker/config.json"
    {
      "auths": {
          "$CI_REGISTRY": {
            "auth": "$credentials"
          }
      }
    }
    EOF

test:
  stage: test
  extends: .bazel_build
  services:
    - redis
  script:
    - echo 'test --test_env=REDIS_URL=redis://redis:6379' >> .bazelrc
    - make test-ci
    - *registry_creds
    - if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then make release-latest; fi

verify:
  stage: test
  extends: .bazel_build
  script:
    - make verify-ci

lint:
  image: golangci/golangci-lint:v1.46.0-alpine
  stage: test
  script:
    # Write the code coverage report to gl-code-quality-report.json
    # and print linting issues to stdout in the format: path/to/file:line description
    - apk --no-cache add jq
    - golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

# You can override the included template(s) by including variable overrides
# See https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables
sast:
  variables:
    # We already run gosec as part of the lint job, no point in running it again here.
    SAST_DEFAULT_ANALYZERS: kubesec
    SCAN_KUBERNETES_MANIFESTS: "true"
  stage: test

push_image_tag:
  stage: push_image
  extends: .bazel_build
  rules:
    # Run this job when the default branch changes and a tag is created manually.
    - if: $CI_COMMIT_TAG
  script:
    - *registry_creds
    - git fetch
    - if [[ $(git branch --remotes --contains "tags/$CI_COMMIT_TAG" | grep "$CI_DEFAULT_BRANCH") ]]; then make release-tag-and-stable; else make release-tag; fi

push_image_manual:
  stage: push_image
  extends: .bazel_build
  rules:
    # Allow creating releases manually for any branch or tag.
    - when: manual
      allow_failure: true
  script:
    - *registry_creds
    - make release-commit

create_release:
  stage: create_release
  rules:
    # Run this job when the default branch changes and a tag is created manually.
    - if: $CI_COMMIT_TAG
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo 'release job'
  release:
    name: '$CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    description: 'Release for tag $CI_COMMIT_TAG'

# Broken, disable for now https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/jobs/2308411911
## see https://docs.gitlab.com/ee/user/project/code_intelligence.html#configuration
#code_navigation:
#  image: sourcegraph/lsif-go:v1
#  allow_failure: true # recommended
#  script:
#    - lsif-go
#  artifacts:
#    reports:
#      lsif: dump.lsif

container_scanning:
  stage: scan
  variables:
    CI_APPLICATION_REPOSITORY: "$CI_REGISTRY_IMAGE/kas"
    CI_APPLICATION_TAG: latest

container_scanning_agentk:
  extends: container_scanning
  variables:
    CI_APPLICATION_REPOSITORY: "$CI_REGISTRY_IMAGE/agentk"
    CI_APPLICATION_TAG: latest

# enable container scanning with https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners
appsec_container_scanning:
  stage: test
  image: ruby:3
  environment:
    name: appsec_container_scanning
    action: prepare
  before_script:
    - cd .gitlab/ci
    - bundle install
  script:
    - ./appsec-container-scan "$CI_REGISTRY_IMAGE/agentk:latest, $CI_REGISTRY_IMAGE/agentk:latest-race, $CI_REGISTRY_IMAGE/kas:latest, $CI_REGISTRY_IMAGE/kas:latest-race" > gl-container-scanning-report.json
  artifacts:
    reports:
      container_scanning: .gitlab/ci/gl-container-scanning-report.json
  rules:
    # Skip on forks, because external contributors can't run this pipeline
    - if: $PIPELINE_TRIGGER_TOKEN
      allow_failure: true
