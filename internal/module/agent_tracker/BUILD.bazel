load("//build:build.bzl", "go_custom_test")
load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:proto.bzl", "go_proto_generate")

go_proto_generate(
    src = "agent_tracker.proto",
    workspace_relative_target_directory = "internal/module/agent_tracker",
    deps = [
        "//internal/module/modshared:proto",
        "@com_google_protobuf//:timestamp_proto",
    ],
)

go_library(
    name = "agent_tracker",
    srcs = [
        "agent_tracker.pb.go",
        "api.go",
        "tracker.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v15/internal/module/agent_tracker",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/module/modshared",
        "//internal/tool/logz",
        "//internal/tool/redistool",
        "@com_github_go_redis_redis_v8//:redis",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_golang_google_protobuf//types/known/timestamppb",
        "@org_uber_go_zap//:zap",
    ],
)

go_custom_test(
    name = "agent_tracker_test",
    srcs = [
        "agent_tracker_test.go",
        "tracker_test.go",
    ],
    embed = [":agent_tracker"],
    deps = [
        "//internal/module/modshared",
        "//internal/tool/redistool",
        "//internal/tool/testing/mock_redis",
        "@com_github_golang_mock//gomock",
        "@com_github_google_go_cmp//cmp",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
        "@org_golang_google_protobuf//proto",
        "@org_golang_google_protobuf//testing/protocmp",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_golang_google_protobuf//types/known/timestamppb",
        "@org_uber_go_zap//zaptest",
    ],
)
